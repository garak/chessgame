<?php

namespace Deployer;

require_once 'recipe/symfony.php';

set('application', 'chess');
set('repository', 'git@bitbucket.org:garak/chessgame.git');
set('git_tty', true);
set('keep_releases', 2);
set('shared_files', []);
set('shared_dirs', ['var/log']);
set('release_name', static fn (): string => \date('ymdHi'));

desc('Update database');
task('deploy:db:update', static function (): void {
    $url = run('grep APP_DATABASE_URL /etc/nginx/sites-available/{{application}}');
    \preg_match('/(mysql:\/\/.*[^;])/', $url, $matches);
    run('APP_DATABASE_URL=\''.$matches[0].'\' {{bin/console}} doctrine:schema:update --force --complete {{console_options}}');
});

desc('Precompile assets');
task('deploy:assets:build_local', static function (): void {
    runLocally('npm run build');
    runLocally('tar zcf assets.tgz public/build/');
    runLocally('mv assets.tgz public/build/');
});
desc('Upload precompiled assets');
task('deploy:assets:upload', static function (): void {
    runLocally('scp public/build/assets.tgz {{user}}@{{hostname}}:{{deploy_path}}');
    run('mv {{deploy_path}}/assets.tgz {{release_path}}; cd {{release_path}}; tar zxf assets.tgz; rm assets.tgz');
    runLocally('rm public/build/assets.tgz');
});

host('production')
    ->setHostname('contabo')
    ->set('deploy_path', '~/sf/{{application}}')
    ->set('remote_user', 'garak')
    ->set('user', 'garak')
;

after('deploy:failed', 'deploy:unlock');
after('deploy:prepare', 'deploy:assets:build_local');
before('deploy:symlink', 'deploy:assets:upload');
before('deploy:symlink', 'deploy:db:update');
