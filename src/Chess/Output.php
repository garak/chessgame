<?php

namespace App\Chess;

use PChess\ChessBundle\HtmlOutput;
use Symfony\Component\Routing\RouterInterface;

final class Output extends HtmlOutput
{
    public function __construct(private readonly RouterInterface $router)
    {
    }

    public function getStartUrl(string $from, mixed $identifier = null): string
    {
        return $this->router->generate('move_start', ['id' => $identifier, 'from' => $from]);
    }

    public function getEndUrl(string $from, string $to, mixed $identifier = null): string
    {
        return $this->router->generate('move_end', ['id' => $identifier, 'from' => $from, 'to' => $to]);
    }

    public function getCancelUrl(mixed $identifier = null): string
    {
        return $this->router->generate('game_show', ['id' => $identifier]);
    }

    public function getPromotionUrl(string $from, string $to, mixed $identifier = null): string
    {
        return $this->router->generate('move_promotion', ['id' => $identifier, 'from' => $from, 'to' => $to]);
    }
}
