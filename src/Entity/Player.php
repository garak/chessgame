<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\UuidInterface;

class Player implements \Stringable
{
    /** @var Collection<int, Game>|null */
    private ?Collection $gamesAsBlack = null;

    /** @var Collection<int, Game>|null */
    private ?Collection $gamesAsWhite = null;

    public function __construct(
        private UuidInterface $id,
        private readonly string $name,
        private readonly string $email,
        private readonly ?string $avatar = null,
        private ?\DateTimeImmutable $login = null
    ) {
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setLogin(\DateTimeImmutable $login): void
    {
        $this->login = $login;
    }

    public function getLogin(): ?\DateTimeImmutable
    {
        return $this->login;
    }

    /**
     * @return Collection<int, Game>
     */
    public function getGames(): Collection
    {
        $bs = null === $this->gamesAsBlack ? [] : $this->gamesAsBlack->toArray();
        $ws = null === $this->gamesAsWhite ? [] : $this->gamesAsWhite->toArray();

        return new ArrayCollection(\array_merge($bs, $ws));
    }
}
