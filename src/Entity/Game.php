<?php

namespace App\Entity;

use App\Entity\ValueObject\Result;
use PChess\Chess\Chess;
use PChess\Chess\Move;
use PChess\Chess\Piece;
use PChess\ChessBundle\Mover;
use Ramsey\Uuid\UuidInterface;

class Game
{
    private string $fen;

    /** @var array<int, string> */
    private array $history;

    private ?Player $black = null;

    private ?Player $white = null;

    public function __construct(
        private UuidInterface $id,
        Player $player,
        string $color = 'w',
        Chess $chess = new Chess(),
        private ?\DateTimeImmutable $start = null,
        private ?\DateTimeImmutable $end = null,
        private ?\DateTimeImmutable $lastMove = null,
        private ?Result $result = null,
    ) {
        $this->fen = $chess->fen();
        $this->history = Mover::getHistoryStrings($chess);
        if ('w' === $color) {
            $this->white = $player;
        } else {
            $this->black = $player;
        }
    }

    public function join(Player $player, string $color): void
    {
        if ('w' === $color) {
            $this->whiteJoin($player);
        } else {
            $this->blackJoin($player);
        }
    }

    public function blackJoin(Player $player): void
    {
        if (null !== $this->black) {
            throw new \DomainException('Black already joined.');
        }
        $this->black = $player;
    }

    public function whiteJoin(Player $player): void
    {
        if (null !== $this->white) {
            throw new \DomainException('White already joined.');
        }
        $this->white = $player;
    }

    public function isWhitePlayer(Player $player): bool
    {
        return null !== $this->white && $this->white->getId()->equals($player->getId());
    }

    public function isBlackPlayer(Player $player): bool
    {
        return null !== $this->black && $this->black->getId()->equals($player->getId());
    }

    public function resign(Player $resigningPlayer): void
    {
        $this->end = new \DateTimeImmutable();
        $this->result = $this->isBlackPlayer($resigningPlayer) ? Result::WHITE_WON : Result::BLACK_WON;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getChess(?Player $player = null): Chess
    {
        $chess = new Chess($this->fen, Mover::getHistoryEntries($this->history));
        if (null !== $player && $this->isBlackPlayer($player)) {
            $chess->board->reverse();
        }

        return $chess;
    }

    public function move(string $from, string $to, ?string $promotion): ?Move
    {
        $chess = $this->getChess();
        $move = $chess->move(['from' => $from, 'to' => $to, 'promotion' => $promotion]);
        $this->setChess($chess);
        if ($chess->gameOver()) {
            if ($chess->inStalemate() || $chess->inDraw()) {
                $this->draw();
            } elseif ($chess->inCheckmate()) {
                $this->win();
            }
        }

        return $move;
    }

    private function setChess(Chess $chess): void
    {
        if (null === $this->start) {
            $this->start = new \DateTimeImmutable();
        }
        $this->lastMove = new \DateTimeImmutable();
        $this->fen = $chess->fen();
        $this->history = Mover::getHistoryStrings($chess);
    }

    public function getBlack(): ?Player
    {
        return $this->black;
    }

    public function getWhite(): ?Player
    {
        return $this->white;
    }

    public function getStart(): ?\DateTimeImmutable
    {
        return $this->start;
    }

    public function getLastMove(): ?\DateTimeImmutable
    {
        return $this->lastMove;
    }

    public function getEnd(): ?\DateTimeImmutable
    {
        return $this->end;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function isStarted(): bool
    {
        return null !== $this->start;
    }

    public function isEnded(): bool
    {
        return null !== $this->end;
    }

    private function draw(): void
    {
        $this->end = new \DateTimeImmutable();
        $this->result = Result::DRAW;
    }

    private function win(): void
    {
        $this->end = new \DateTimeImmutable();
        $this->result = Piece::WHITE === $this->getChess()->turn ? Result::WHITE_WON : Result::BLACK_WON;
    }
}
