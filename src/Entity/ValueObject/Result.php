<?php

namespace App\Entity\ValueObject;

enum Result: int
{
    case DRAW = 0;
    case BLACK_WON = 1;
    case WHITE_WON = 2;

    public function label(): string
    {
        return match ($this) {
            self::DRAW => 'draw',
            self::BLACK_WON => 'black won',
            self::WHITE_WON => 'white won',
        };
    }
}
