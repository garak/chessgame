<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:chess:autoplay', description: 'See a chess game playing itself.')]
final class AutoPlayCommand extends AbstractCommand
{
    protected function configure(): void
    {
        $this
            ->addArgument('wait', InputArgument::OPTIONAL, 'Waiting time between moves', '500000')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $chess = $this->provider->getChess();
        do {
            $output->write("\033\143");
            $this->getBoard($chess, $output)->render();
            $this->getTurn($chess, $output);
            $this->getHistory($chess, $output)->render();
            $moves = $chess->moves();
            $count = \count($moves);
            if ($count < 1) {
                throw new \UnexpectedValueException('no enough moves left');
            }
            $move = $moves[\random_int(0, $count - 1)];
            if (null === $move->san) {
                throw new \UnexpectedValueException('no SAN for move');
            }
            $chess->move($move->san);
            \usleep((int) $input->getArgument('wait'));
        } while (!$chess->gameOver());
        $output->write("\033\143");
        $this->getBoard($chess, $output)->render();
        $this->getOutcome($chess, $output);
        $this->getHistory($chess, $output)->render();

        return self::SUCCESS;
    }
}
