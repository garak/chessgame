<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

#[AsCommand(name: 'app:chess:play', description: 'Play a chess game.')]
final class PlayCommand extends AbstractCommand
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $chess = $this->provider->getChess();
        do {
            $output->write("\033\143");
            $this->getBoard($chess, $output)->render();
            $this->getTurn($chess, $output);
            $this->getHistory($chess, $output)->render();
            $question = new Question('<question>Move (e.g. "e4" or "e2-e4"). Q for quit.</question>');
            $move = $this->getQuestionHelper()->ask($input, $output, $question);
            if ($move) {
                if (\in_array($move, ['q', 'Q', 'quit', 'Quit', 'QUIT'], true)) {
                    return 0;
                }
                if (\strpos($move, '-') > 0) {
                    [$from, $to] = \explode('-', $move);
                    $chess->move(['from' => $from, 'to' => $to]);
                } else {
                    $chess->move($move);
                }
            }
        } while (!$chess->gameOver());
        $output->write("\033\143");
        $this->getBoard($chess, $output)->render();
        $this->getOutcome($chess, $output);
        $this->getHistory($chess, $output)->render();

        return self::SUCCESS;
    }
}
