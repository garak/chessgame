<?php

namespace App\Command;

use PChess\Chess\Board;
use PChess\Chess\Chess;
use PChess\Chess\Entry;
use PChess\ChessBundle\SimpleChessProvider as ChessProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCommand extends Command
{
    public function __construct(protected readonly ChessProvider $provider)
    {
        parent::__construct();
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
        $outputStyle1 = new OutputFormatterStyle('black', 'white', ['bold']);
        $outputStyle2 = new OutputFormatterStyle('white', 'blue', ['bold']);
        $output->getFormatter()->setStyle('w', $outputStyle1);
        $output->getFormatter()->setStyle('b', $outputStyle2);
    }

    protected function getBoard(Chess $chess, OutputInterface $output): Table
    {
        $rows = [];
        $ln = 0;
        /** @var int $i */
        foreach ($chess->board as $i => $piece) {
            $ln = Board::rank($i) * 10;
            if (0 === Board::file($i)) {
                $rows[$ln][] = \sprintf('<info>%s</info>', \substr('87654321', Board::rank($i), 1));
            }
            $rows[$ln][] = $chess->board[$i] ? \sprintf('<%1$s>%2$s%3$s</%1$s>', $chess->board[$i]->getColor(), $chess->board[$i]->toUnicode(), "\x20") : " \x20";
            if (($i + 1) & 0x88) {
                $rows[$ln + 1][] = new TableSeparator(['colspan' => 9]);
            }
        }
        unset($rows[$ln + 1]);
        $headers = \array_map(static fn (string $l) => \sprintf('<info>%s</info>', $l), [-1 => ''] + \range('a', 'h'));
        $table = new Table($output);
        $table->setHeaders($headers);
        $table->setRows($rows);
        $table->setStyle('box-double');

        return $table;
    }

    protected function getHistory(Chess $chess, OutputInterface $output): Table
    {
        $pieces = \array_chunk($chess->getHistory()->getEntries(), 20);
        $table = new Table($output);
        $table->setHeaders(['History']);
        foreach ($pieces as $piece) {
            $table->addRow([\implode(' ', \array_map(static fn (Entry $entry): ?string => $entry->move->san, $piece))]);
        }
        $table->setStyle('box-double');

        return $table;
    }

    protected function getTurn(Chess $chess, OutputInterface $output): void
    {
        $output->writeln(\sprintf('<comment>Turn: %s</comment>', 'w' === $chess->turn ? 'white' : 'black'));
    }

    protected function getOutcome(Chess $chess, OutputInterface $output): void
    {
        $outcome = 'unknown';
        if ($chess->inStalemate()) {
            $outcome = 'Stale mate';
        } elseif ($chess->inCheckmate()) {
            $outcome = 'Check mate';
        } elseif ($chess->inDraw()) {
            $outcome = 'Draw';
        } elseif ($chess->insufficientMaterial()) {
            $outcome = 'Insufficient material';
        }

        $output->writeln(\sprintf('<comment>Outcome: %s</comment>', $outcome));
    }

    protected function getQuestionHelper(): QuestionHelper
    {
        return $this->getHelper('question');    // @phpstan-ignore-line
    }
}
