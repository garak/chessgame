<?php

namespace App\Twig;

use App\Entity\Player;
use App\Repository\PlayerRepository;
use Imagine\Gd\Imagine;
use PChess\Chess\Chess;
use PChess\Chess\Output\ImagineOutput;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Extension\RuntimeExtensionInterface;

final readonly class UserExtensionRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private PlayerRepository $repository,
        private string $rootDir
    ) {
    }

    public function getAvatar(): ?string
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }
        /** @var \App\Security\User\User $user */
        $user = $token->getUser();
        if (null === $player = $this->repository->findByEmail($user->getUserIdentifier())) {
            return null;
        }

        return $player->getAvatar();
    }

    public function getPlayer(): Player
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            throw new \RuntimeException('Unauthenticated.');
        }
        /** @var \App\Security\User\User $user */
        $user = $token->getUser();
        if (null === $player = $this->repository->findByEmail($user->getUserIdentifier())) {
            throw new \RuntimeException('No player found.');
        }

        return $player;
    }

    public function getImage(Chess $chess, int $size = 64): string
    {
        try {
            $imagine = new Imagine();
            $output = new ImagineOutput($imagine, $this->rootDir.'/public/img/', $size);

            return 'data:image/png;base64, '.\base64_encode($output->render($chess));
        } catch (\RuntimeException) {
            return 'iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAM1BMVEX///8AAADS0tKdnZ2YmJgEBATy8vLMzMzPz8/7+/v4+PgICAhjY2Oenp6kpKSVlZVeXl670ZgEAAABi0lEQVR4nO3d22rbUBiEUW8rthXHPbz/05ZcpAQaaBmk2Uq61gNs5hPo+j+dAAAAAAAAAIDP5nn2gD9suuh+va3no1lv1/t2ies4onW7wP+g8Dw75kNnhQoVTrdH4eXV1KrxbsEuhS+zwt552bVwPL4tkz3GvoXfN3w1s+xaOMay4auZH7+3fNXCJ4URhU0KMwqbFGYUNinMKGxSmFHYpDCjsElhRmGTwozCJoUZhU0KMwqbFGYUNinMKGxSmFHYpDCjsElhRmGTwozCJoUZhU0KMwqbFGYUNinMKGxSmFHYpDCjsElhRmGTwozCJoUZhU0KMwqbFGYUNinMKGxSmFHYpDCjsElhRmGTwozCJoUZhU0KMwqbFGYUNinMKGxSmFHYpDCjsElhRmGTwozCJoUZhU0KM8cq3Pc64Fe/f3gZj+VpruXnvoVHuEO676XVyxhzb8m+Lnj7zPv8h0eiUKHC+bYsXGfHfGhV+O/u19t6Ppr1dr1vl3g6PW/52CaOtwgAAAAAAAAA+JtfCAAhFsMSvRMAAAAASUVORK5CYII=';
        }
    }
}
