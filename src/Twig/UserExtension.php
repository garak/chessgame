<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class UserExtension extends AbstractExtension
{
    /**
     * @codeCoverageIgnore
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('avatar', [UserExtensionRuntime::class, 'getAvatar']),
            new TwigFunction('player', [UserExtensionRuntime::class, 'getPlayer']),
            new TwigFunction('image', [UserExtensionRuntime::class, 'getImage']),
        ];
    }
}
