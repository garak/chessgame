<?php

namespace App\Controller;

use App\Repository\GameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

final class DefaultController extends AbstractController
{
    #[Route(path: '/', name: 'homepage', methods: 'GET', priority: 1)]
    public function index(GameRepository $repoGame): Response
    {
        $games = $repoGame->findAll();

        return $this->render('game/index.html.twig', ['games' => $games]);
    }
}
