<?php

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

/**
 * @codeCoverageIgnore
 */
final class AuthController extends AbstractController
{
    // see this URL in GoogleAuthenticator class
    #[Route(path: '/connect/google', name: 'connect_google_start')]
    public function connectGoogle(GoogleClient $client): Response
    {
        return $client->redirect();
    }

    // see this route name in GoogleAuthenticator class and in knpu_oauth2_client.yaml
    #[Route(path: '/connect/google/check', name: 'connect_google_check', schemes: ['https'])]
    public function connectGoogleCheck(GoogleClient $client): void
    {
    }
}
