<?php

namespace App\Controller;

use App\Repository\GameRepository;
use PChess\ChessBundle\Mover;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/game/', name: 'move_')]
final class MoveController extends AbstractController
{
    #[Route(path: '{id}/move/{from}', name: 'start', methods: 'GET')]
    public function moveStart(string $id, string $from, GameRepository $repoGame): Response
    {
        $game = $repoGame->get($id);
        $this->denyAccessUnlessGranted('move_start', $game);
        if ([] === $moves = Mover::getAllowedMoves($game->getChess(), $from)) {
            throw new AccessDeniedHttpException('Invalid move start.');
        }

        return $this->render('game/game.html.twig', ['id' => $id, 'game' => $game, 'from' => $from, 'moves' => $moves]);
    }

    #[Route(path: '{id}/move/{from}/{to}/{promotion}', name: 'end', defaults: ['promotion' => null], methods: 'GET')]
    public function moveEnd(string $id, GameRepository $repoGame, string $from, string $to, ?string $promotion = null): Response
    {
        $game = $repoGame->get($id);
        $this->denyAccessUnlessGranted('move_end', $game);
        if (null === $game->move($from, $to, $promotion)) {
            throw new AccessDeniedHttpException('Invalid move.');
        }
        $repoGame->add($game);

        return $this->redirectToRoute('game_show', ['id' => $id]);
    }

    #[Route(path: 'promotion/{from}/{to}', name: 'promotion', methods: 'GET')]
    public function promotion(string $id, GameRepository $repoGame, string $from, string $to): Response
    {
        $game = $repoGame->get($id);
        $this->denyAccessUnlessGranted('move_promotion', $game);

        return $this->render('game/game.html.twig', ['id' => $id, 'game' => $game, 'from' => $from, 'to' => $to]);
    }
}
