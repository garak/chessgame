<?php

namespace App\Controller;

use App\Entity\Game;
use App\Repository\GameRepository;
use App\Repository\PlayerRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/game/', name: 'game_')]
final class GameController extends AbstractController
{
    #[Route(path: 'new/{color}', name: 'new', methods: 'GET')]
    public function new(string $color, GameRepository $repoGame, PlayerRepository $repoPlayer): Response
    {
        if (null === $player = $repoPlayer->findByEmail($this->getUserEmail())) {
            throw $this->createAccessDeniedException();
        }
        $game = new Game(Uuid::uuid4(), $player, $color);
        $repoGame->add($game);

        return $this->redirectToRoute('game_show', ['id' => $game->getId()]);
    }

    #[Route(path: '{id}', name: 'show', methods: 'GET')]
    public function game(string $id, GameRepository $repoGame): Response
    {
        $game = $repoGame->get($id);

        return $this->render('game/game.html.twig', ['id' => $id, 'game' => $game]);
    }

    #[Route(path: '{id}/join/{color}', name: 'join', methods: 'GET')]
    public function join(string $id, string $color, GameRepository $repoGame, PlayerRepository $repoPlayer): Response
    {
        if (null === $player = $repoPlayer->findByEmail($this->getUserEmail())) {
            throw $this->createAccessDeniedException();
        }
        $game = $repoGame->get($id);
        $game->join($player, $color);
        $repoGame->add($game);

        return $this->redirectToRoute('game_show', ['id' => $id]);
    }

    #[Route(path: '{id}/resign', name: 'resign', methods: 'GET')]
    public function resign(string $id, GameRepository $repoGame, PlayerRepository $repoPlayer): Response
    {
        $game = $repoGame->get($id);
        $this->denyAccessUnlessGranted('resign', $game);
        $game->resign($repoPlayer->getByEmail($this->getUserEmail()));
        $repoGame->add($game);

        return $this->redirectToRoute('game_show', ['id' => $id]);
    }

    private function getUserEmail(): string
    {
        if (null === $user = $this->getUser()) {
            throw $this->createAccessDeniedException();
        }

        return $user->getUserIdentifier();
    }
}
