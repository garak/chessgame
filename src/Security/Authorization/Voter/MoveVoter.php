<?php

namespace App\Security\Authorization\Voter;

use App\Entity\Game;
use App\Repository\PlayerRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends Voter<string, Game>
 */
final class MoveVoter extends Voter
{
    /** @var array<int, string> */
    private const array ATTRIBUTES = ['move_start', 'move_end', 'move_promotion', 'resign'];

    public function __construct(private readonly PlayerRepository $repository)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return $subject instanceof Game && \in_array($attribute, self::ATTRIBUTES, true);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        /** @var \App\Security\User\User $user */
        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return false;
        }
        if (null === $player = $this->repository->findByEmail($user->getUserIdentifier())) {
            return false;
        }
        // any player can resign at any time
        if ('resign' === $attribute) {
            return $subject->isWhitePlayer($player) || $subject->isBlackPlayer($player);
        }
        if ('w' === $subject->getChess()->turn) {
            return $subject->isWhitePlayer($player);
        }
        // black cannot do first move
        if (null === $subject->getStart()) {
            return false;
        }

        return $subject->isBlackPlayer($player);
    }
}
