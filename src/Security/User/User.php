<?php

namespace App\Security\User;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final readonly class User implements UserInterface, EquatableInterface, \Stringable
{
    /**
     * @param array<string> $roles
     */
    public function __construct(private string $email, private array $roles = ['ROLE_USER'])
    {
    }

    public function __toString(): string
    {
        return $this->email;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function eraseCredentials(): void
    {
        // not needed, but required by the interface
    }

    public function isEqualTo(UserInterface $user): bool
    {
        return $user->getUserIdentifier() === $this->email;
    }

    /**
     * @return array{string, array<int, string>}
     */
    public function __serialize(): array
    {
        return [$this->email, $this->roles];
    }

    /**
     * @param array{string, array<int, string>} $data
     */
    public function __unserialize(array $data): void
    {
        [$this->email, $this->roles] = $data;
    }
}
