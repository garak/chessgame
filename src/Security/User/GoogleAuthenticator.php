<?php

namespace App\Security\User;

use App\Entity\Player;
use App\Repository\PlayerRepository;
use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

final class GoogleAuthenticator extends OAuth2Authenticator
{
    public function __construct(private readonly GoogleClient $client, private readonly PlayerRepository $repository)
    {
    }

    public function supports(Request $request): bool
    {
        // see route name in config/routes.yaml
        return 'connect_google_check' === $request->attributes->get('_route');
    }

    public function authenticate(Request $request): Passport
    {
        $accessToken = $this->fetchAccessToken($this->client);

        return new SelfValidatingPassport(
            new UserBadge($accessToken->getToken(), function () use ($accessToken): UserInterface {
                /** @var \League\OAuth2\Client\Provider\GoogleUser $googleUser */
                $googleUser = $this->client->fetchUserFromToken($accessToken);
                if (null === $email = $googleUser->getEmail()) {
                    throw new UserNotFoundException('unknown');
                }
                if (null === $player = $this->repository->findByEmail($email)) {
                    $player = new Player(
                        Uuid::uuid4(),
                        $googleUser->getName(),
                        $email,
                        $googleUser->getAvatar(),
                        new \DateTimeImmutable(),
                    );
                } else {
                    $player->setLogin(new \DateTimeImmutable());
                }
                $this->repository->add($player);

                return new User($player->getEmail());
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): Response
    {
        return new RedirectResponse('/');
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $message = \strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }
}
