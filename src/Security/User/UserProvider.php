<?php

namespace App\Security\User;

use App\Repository\PlayerRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @implements UserProviderInterface<User>
 */
final readonly class UserProvider implements UserProviderInterface
{
    public function __construct(private PlayerRepository $repository)
    {
    }

    public function loadUserByIdentifier(string $identifier): User
    {
        if (null === ($player = $this->repository->findByEmail($identifier))) {
            throw new UserNotFoundException(\sprintf('Player "%s" not found', $identifier));
        }
        $roles = ['ROLE_USER'];

        return new User($player->getEmail(), $roles);
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }

    public function refreshUser(UserInterface $user): User
    {
        $class = $user::class;
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(\sprintf('Unsupported "%s" instance.', $class));
        }

        return new User($user->getUserIdentifier());
    }
}
