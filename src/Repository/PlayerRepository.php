<?php

namespace App\Repository;

use App\Entity\Player;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @extends EntityRepository<Player>
 */
final class PlayerRepository extends EntityRepository
{
    public function add(Player $player): void
    {
        $this->getEntityManager()->persist($player);
        $this->getEntityManager()->flush();
    }

    public function getByEmail(string $email): Player
    {
        if (null !== $player = $this->findByEmail($email)) {
            return $player;
        }
        throw new \DomainException('Player not found');
    }

    public function findByEmail(string $email): ?Player
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function get(string $id): Player
    {
        if (null === $player = $this->find($id)) {
            throw new NoResultException();
        }

        return $player;
    }
}
