<?php

namespace App\Repository;

use App\Entity\Game;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * @extends EntityRepository<Game>
 */
final class GameRepository extends EntityRepository
{
    public function add(Game $game): void
    {
        $this->getEntityManager()->persist($game);
        $this->getEntityManager()->flush();
    }

    public function get(string $id): Game
    {
        if (null === $game = $this->find($id)) {
            throw new NoResultException();
        }

        return $game;
    }
}
