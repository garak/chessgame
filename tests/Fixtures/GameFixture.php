<?php

namespace Tests\Fixtures;

use App\Entity\Game;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

final class GameFixture extends AbstractFixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var \App\Entity\Player $pl1 */
        $pl1 = $this->getReference('player1');
        /** @var \App\Entity\Player $pl2 */
        $pl2 = $this->getReference('player2');

        $game1 = new Game(Uuid::uuid4(), $pl1);
        $manager->persist($game1);

        $game2 = new Game(Uuid::uuid4(), $pl1);
        $manager->persist($game2);
        $game2->join($pl2, 'b');

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [PlayerFixture::class];
    }
}
