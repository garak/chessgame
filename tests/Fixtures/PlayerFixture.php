<?php

namespace Tests\Fixtures;

use App\Entity\Player;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

final class PlayerFixture extends AbstractFixture
{
    public function load(ObjectManager $manager): void
    {
        $pl1 = new Player(Uuid::uuid4(), 'Max', 'garakkio@gmail.com');
        $manager->persist($pl1);
        $this->addReference('player1', $pl1);

        $pl2 = new Player(Uuid::uuid4(), 'John', 'john@gmail.com');
        $manager->persist($pl2);
        $this->addReference('player2', $pl2);

        $manager->flush();
    }
}
