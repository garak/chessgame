<?php

namespace Tests\Command;

use App\Command\PlayCommand;
use PHPUnit\Framework\Attributes\Group;

#[Group('functional'), Group('command')]
final class PlayCommandTest extends AbstractCommandTestCase
{
    public function testQuit(): void
    {
        $output = self::commandTest('app:chess:play', new PlayCommand(self::getProvider()), [], [], ['e2-e4', 'q']);
        self::assertStringContainsString(' ♙ ', $output);
    }

    public function testPlayFullGame(): void
    {
        $moves = ['f3', 'e6', 'g4', 'd8-h4'];
        $output = self::commandTest('app:chess:play', new PlayCommand(self::getProvider()), [], [], $moves);
        self::assertStringContainsString('Check mate', $output);
    }
}
