<?php

namespace Tests\Command;

use Beelab\TestBundle\Test\WebTestCase;
use PChess\ChessBundle\SimpleChessProvider as ChessProvider;

abstract class AbstractCommandTestCase extends WebTestCase
{
    protected static function getProvider(): ChessProvider
    {
        $provider = self::$container?->get('test.chess_provider');
        \assert($provider instanceof ChessProvider);

        return $provider;
    }
}
