<?php

namespace Tests\Command;

use App\Command\AutoPlayCommand;
use PHPUnit\Framework\Attributes\Group;

#[Group('functional'), Group('command')]
final class AutoPlayCommandTest extends AbstractCommandTestCase
{
    public function testExecute(): void
    {
        $output = self::commandTest('app:chess:autoplay', new AutoPlayCommand(self::getProvider()), ['wait' => 0]);
        self::assertStringContainsString(' ♙ ', $output);
    }
}
