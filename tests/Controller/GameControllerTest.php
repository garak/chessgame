<?php

namespace Tests\Controller;

use Beelab\TestBundle\Test\WebTestCase;
use PHPUnit\Framework\Attributes\Group;

#[Group('functional')]
final class GameControllerTest extends WebTestCase
{
    public function testHomepage(): void
    {
        self::$client->request('GET', '/');
        self::assertResponseIsSuccessful();
    }

    public function testCreateNewGame(): void
    {
        self::login('john@gmail.com');
        self::$client->request('GET', '/');
        self::clickLinkBySelectorText('black');
        self::$client->followRedirect();
        self::assertResponseIsSuccessful();
    }

    public function testJoinGame(): void
    {
        self::login('john@gmail.com');
        self::$client->request('GET', '/');
        self::clickLinkBySelectorText('view', 'tr:not(:contains("John"))');
        self::assertResponseIsSuccessful();
        self::clickLinkBySelectorText('join');
        self::$client->followRedirect();
        self::assertResponseIsSuccessful();
    }

    public function testMakeAMove(): void
    {
        self::login('garakkio@gmail.com');
        self::$client->request('GET', '/');
        self::clickLinkBySelectorText('view', 'tr:contains("John")');
        self::clickMove('e2', 'e4');
        self::$client->followRedirect();
        self::assertResponseIsSuccessful();
    }

    public function testResign(): void
    {
        self::login('garakkio@gmail.com');
        self::$client->request('GET', '/');
        self::clickLinkBySelectorText('view', 'tr:contains("John")');
        self::clickMove('d2', 'd4');
        self::$client->followRedirect();
        self::clickLinkBySelectorText('resign');
        self::$client->followRedirect();
        self::assertResponseIsSuccessful();
    }

    private static function clickMove(string $from, string $to): void
    {
        $linkNode = self::$client->getCrawler()->filter('a[href$="'.$from.'"]');
        self::$client->click($linkNode->link());
        $linkNode = self::$client->getCrawler()->filter('a[href$="'.$to.'"]');
        self::$client->click($linkNode->link());
    }
}
