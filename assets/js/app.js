import Dropdown from 'bootstrap/js/src/dropdown';

// service worker
if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/service-worker.js").then(reg => console.log("reg is " + reg.scope));
}

// dropdown
let dropDowns = document.querySelectorAll("[data-bs-toggle='dropdown']");
Array.from(dropDowns, (dropDown) => {
    new Dropdown(dropDown);
});

// scroll history to bottom
let ol = document.querySelector("ol");
if (ol) {
    ol.scrollTop = ol.scrollHeight;
}
