# Chess game

## Setup

* run `docker compose build`
* run `make start`
* run `docker compose exec php composer install`
* run `make npm`
* run `make asset` (you can stop the process after assets generation)
* override variables in your `docker-composer.override.yaml` file

## Run the application

* open a browser to [chessgame.local:8080][1] (the domain needs to be hosted).
  Alternatively, you can run an instance of ngrok (see next paragraph).
* on a terminal, run `make console "app:chess:play"`.
  You can also see the computer playing a game with itself, running `make console "app:chess:autoplay"`.

## ngrok

* execute `ngrok http 8080`
* note down the forwarding address, e.g. `https://fa24aab8209b.ngrok-free.app`
* configure that address in the Google dashboard, "Credentials" section, then "OAuth 2.0 Client ID"
* use that address in place of `http://chessgame.local:8080`

## Stop

* run `make stop`

## Tests

* run `make test` (or `make coverage`)

[1]: http://chessgame.local:8080
